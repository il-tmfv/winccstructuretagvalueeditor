﻿using System;
using System.Linq;
using NLog;

namespace WinCCStructureTagValueEditor.Infrastructure
{
    class TagEditor
    {
        public delegate void TagEditorEventHandler(string message);

        public event TagEditorEventHandler OnError;
        public event TagEditorEventHandler OnSuccess;

        private Logger _logger;

        private int _endingsCount = 0;

        private string[] _tagValues;
        private string[] _tagEndings;

        public string[] TagValues
        {
            get
            {
                return _tagValues;
            }
            private set
            {
                _tagValues = value;
            }
        }

        public string[] TagEndings
        {
            get
            {
                return _tagEndings;
            }
            private set
            {
                _tagEndings = value;
            }
        }

        private HMIGENOBJECTSLib.HMIGO _HMIGOObject = null;

        public TagEditor()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        private string[] StringToArray(string str, string delimeter)
        {
            string[] result;

            result = str.Split(new string[] { delimeter }, StringSplitOptions.RemoveEmptyEntries);
            result = result.Select(s => s.Trim()).ToArray();

            return result;
        }

        public bool Initialize(string tagValues, string tagEndings)
        {
            if (String.IsNullOrWhiteSpace(tagValues))
            {
                OnError("Нет данных по значениям тегов");
                return false;
            }

            if (String.IsNullOrWhiteSpace(tagEndings))
            {
                OnError("Нет данных по окончаниям тегов");
                return false;
            }

            TagEndings = StringToArray(tagEndings, "\r\n");
            _endingsCount = TagEndings.Length;
            TagValues = StringToArray(tagValues, "\r\n");


            try
            {
                _HMIGOObject = new HMIGENOBJECTSLib.HMIGO();
            }
            catch
            {
                OnError("Не удалось создать HMIGOObject");
                return false;
            }

            return true;
        }

        public void DoWork()
        {
            string[] tmpStrings;
            string tagNameStart;
            string tagName;
            string tagValue;
            bool IsAllEntriesOk = true;
            int i = 1;

            foreach (var item in TagValues)
            {
                i = 1;
                tmpStrings = StringToArray(item, "\t");
                if (tmpStrings.Length != (_endingsCount + 1)) //почему-то значений пришло меньше или больше чем нужно
                {
                    _logger.Error("Размерности массивов не совпадают (кол-во тегов и кол-во значений)");
                    IsAllEntriesOk = false;
                    continue;
                }
                tagNameStart = tmpStrings[0];
                foreach (var ending in TagEndings)
                {
                    tagName = tagNameStart + "." + ending;
                    tagValue = tmpStrings[i];
                    _logger.Info("Начинается редактирование тег-значение: " + tagName + "-" + tagValue);
                    _HMIGOObject.GetTag(tagName);
                    _HMIGOObject.TagStart = tagValue;
                    _HMIGOObject.CommitTag();
                    _logger.Info("Закончено редактирование тег-значение: " + tagName + "-" + tagValue);
                    i++;
                }
            }

            _HMIGOObject = null;
            if (IsAllEntriesOk)
            {
                _logger.Info("Всё выполнено!");
                OnSuccess("Выполнено!");
            }
            else
            {
                _logger.Info("Всё выполнено! Однако не все записи были правильными");
                OnSuccess("Выполнено! Однако не все записи были правильными");
            }
        }
    }
}

