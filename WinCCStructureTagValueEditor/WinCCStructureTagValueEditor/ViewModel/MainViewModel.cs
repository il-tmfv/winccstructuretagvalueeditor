﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Windows.Media;
using WinCCStructureTagValueEditor.Infrastructure;

namespace WinCCStructureTagValueEditor.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            this.TagValues = "";
            this.TagEndings = "code\r\nfullcode\r\nlabel\r\nunit";
            this.Message = "Готов";
            this.IsControlsEnabled = true;    
        }

        #region MessageColor
        /// <summary>
        /// The <see cref="MessageColor" /> property's name.
        /// </summary>
        public const string MessageColorPropertyName = "MessageColor";

        private Brush _messageColor = null;

        /// <summary>
        /// Sets and gets the MessageColor property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public Brush MessageColor
        {
            get
            {
                return _messageColor;
            }

            set
            {
                if (_messageColor == value)
                {
                    return;
                }

                _messageColor = value;
                RaisePropertyChanged(MessageColorPropertyName);
            }
        }
        #endregion

        #region Message
        /// <summary>
        /// The <see cref="Message" /> property's name.
        /// </summary>
        public const string MessagePropertyName = "Message";

        private string _message = null;

        /// <summary>
        /// Sets and gets the Message property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string Message
        {
            get
            {
                return _message;
            }

            set
            {
                if (_message == value)
                {
                    return;
                }

                _message = value;
                RaisePropertyChanged(MessagePropertyName);
            }
        }
        #endregion
        
        #region TagEndings
        /// <summary>
        /// The <see cref="TagEndings" /> property's name.
        /// </summary>
        public const string TagEndingsPropertyName = "TagEndings";

        private string _tagEndings = null;

        /// <summary>
        /// Sets and gets the TagEndings property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string TagEndings
        {
            get
            {
                return _tagEndings;
            }

            set
            {
                if (_tagEndings == value)
                {
                    return;
                }

                _tagEndings = value;
                RaisePropertyChanged(TagEndingsPropertyName);
            }
        }
        #endregion
        
        #region TagValues
        /// <summary>
        /// The <see cref="TagValues" /> property's name.
        /// </summary>
        public const string TagValuesPropertyName = "TagValues";

        private string _tagValues = null;

        /// <summary>
        /// Sets and gets the TagValues property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string TagValues
        {
            get
            {
                return _tagValues;
            }

            set
            {
                if (_tagValues == value)
                {
                    return;
                }

                _tagValues = value;
                RaisePropertyChanged(TagValuesPropertyName);
            }
        }
        #endregion

        #region IsControlsEnabled
        /// <summary>
        /// The <see cref="IsControlsEnabled" /> property's name.
        /// </summary>
        public const string IsControlsEnabledPropertyName = "IsControlsEnabled";

        private bool _isControlsEnabled = true;

        /// <summary>
        /// Sets and gets the IsControlsEnabled property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public bool IsControlsEnabled
        {
            get
            {
                return _isControlsEnabled;
            }

            set
            {
                if (_isControlsEnabled == value)
                {
                    return;
                }

                _isControlsEnabled = value;
                RaisePropertyChanged(IsControlsEnabledPropertyName);
            }
        }
        #endregion

        #region ButtonClick
        private RelayCommand _buttonClick;

        /// <summary>
        /// Gets the ButtonClick.
        /// </summary>
        public RelayCommand ButtonClick
        {
            get
            {
                return _buttonClick
                    ?? (_buttonClick = new RelayCommand(
                    () =>
                    {
                        EditTagsFunction();
                    }));
            }
        }

        private void EditTagsFunction()
        {
            this.IsControlsEnabled = false;
            //_cts = new CancellationTokenSource();
            TagEditor te = new TagEditor();
            te.OnError += te_OnError;
            te.OnSuccess += te_OnSuccess;
            if (te.Initialize(TagValues, TagEndings))
            {
                try
                {
                    //await Task.Run(() => te.DoWork(), _cts.Token);
                    te.DoWork();
                }
                catch (Exception ex)
                {
                    Message = "Ошибка:" + ex.ToString();
                    MessageColor = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                }
                finally
                {
                    this.IsControlsEnabled = true;
                }
            }

            this.IsControlsEnabled = true;
            te = null;
        }
        #endregion

        void te_OnSuccess(string message)
        {
            Message = message;
            MessageColor = new SolidColorBrush(Color.FromRgb(0, 200, 0));
        }

        private void te_OnError(string message)
        {
            Message = message;
            MessageColor = new SolidColorBrush(Color.FromRgb(255, 0, 0));
        }
        
        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}
    }
}